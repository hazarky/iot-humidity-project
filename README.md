# Welcome to the PiSense Humidity Source Code
## Pre-requisites
- Raspberry Pi 
- Pi Sense Humidity Sensor Board
- Python 3
- A google account modified to be able to use custom apps
- Pip3 with the package: SenseHat and datetime installed
## Getting Started
1. On the Pi, pull the github repo `git clone gitlab.com/hazarky/iot-humidity-project`
2. Either Create or Use a Google account (use this link to help figure out the email: https://www.bc-robotics.com/tutorials/sending-email-using-python-raspberry-pi/)
3. In the folder, run humid.py `python3 humid.py`

To have this program run periodically, I would advise using cronjobs. cronjob is a easy way on linux to routinely run things on that system. to find the best time for you to run a cronjob, use crontab guru [link](https://crontab.guru/).
